# How to setup a Q Fullnode

## Setup your Server

You must prepare your server / machine to begin. One possibility is to use a local machine, alternatively you can use a cloud instance on AWS for example. There is a good external tutorial on how to get started with Ethereum on AWS. You can use this [tutorial](https://medium.com/@pilankar.akshay3/how-to-setup-a-ethereum-poa-private-proof-of-authority-ethereum-network-network-on-amazon-aws-5fdf56d2ad93) as a basic reference.

## Get basic Configuration

Clone the repository and go to the `/fullnode` directory

/// details | Linux
If you use Graphical User Interface(GUI), [open terminal](https://www.geeksforgeeks.org/how-to-open-terminal-in-linux/)

1. Press `CTRL + ALT + T` to open the terminal
2. Or open Show Application in the Dash Bar & find out Terminal Application.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/mainnet-public-tools
```
5. Go to the `/fullnode` directory
```bash
cd  "mainnet-public-tools/fullnode"
```
///

/// details | Windows (if you don't have git installed)
Open [PowerShell](https://learn.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7.5)

1. Press `Windows+R` to open the "Run" tool
2. Type "powershell" and press Enter to open terminal
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DISK:\YOUR_DIRECTORY"
```
If you work with cmd instead of PowerShell, use command:
```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY"
```
Replace YOUR_DISK and YOUR_DIRECTORY with real names.<br>
You can print the current working directory and its contents using the command:
```bash
dir
```

4. Clone the repository:
```
# Download the contents of the Git repository
Invoke-WebRequest -Uri https://gitlab.com/q-dev/mainnet-public-tools/-/archive/master/mainnet-public-tools-master.zip -OutFile mainnet-public-tools-master.zip

# Extract the contents of the ZIP file
Expand-Archive -Path mainnet-public-tools-master.zip -DestinationPath .

# Remove the ZIP file
Remove-Item -Path mainnet-public-tools-master.zip
```

5. Go to the `/fullnode` directory

```bash
cd "mainnet-public-tools\fullnode"
```
///

/// details | MacOS
[Open Terminal on Mac](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac)

1. Click the Launchpad icon in the Dock, type Terminal in the search field, then click Terminal.
2. In the Finder, open the /Applications/Utilities folder, then double-click Terminal.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/mainnet-public-tools
```
5. Go to the `/fullnode` directory
```bash
cd  "mainnet-public-tools/fullnode"
```
///
<br>
This directory contains the `docker-compose.yaml` file for quick launching of the full node with preconfigurations on rpc, blockchain explorer using `.env` file (which can be created from `.env.example` file).
## Configure Ports

Choose ports (or leave default values) for node rpc api and blockchain explorer by copying `.env.example` to `.env` and editing the file.

/// details | Linux, macOS, other Unix-like systems
```bash
cp .env.example .env
nano .env
```
After editing the ports, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
# This will copy the .env.example file to a new file named .env.
copy ".\env.example"  ".\env"

#This will open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\env
```
Save changes with `Ctrl+S` and close notepad.
///
<br>
Replace existing ports.
```bash
# the port you want to use for p2p communication (default is 30314)
EXT_PORT=30314

# the port you want to use for explorer (default is 8081)
EXPLORER_PORT=8081
```

## Launch Node

Launch the node by executing the following command from `/fullnode` directory:

```bash
docker-compose up -d
```

## Verify your Installation

After node is launched, it starts syncing with network.

The setup includes a local blockchain explorer. You can browse blocks, transactions and accounts via your browser by opening the url `http://localhost:PORT`
where `PORT` is the number you chose above for EXPLORER_PORT, e.g. `http://localhost:8080`

You can check your nodes real-time logs with the following command:

```bash
docker-compose logs -f --tail "100"
```

## Find additional peers

In case your client can't connect with the default configuration, we recommend that you add an additional flag referring to one of our additional peers (`$BOOTNODE1_ADDR`, `$BOOTNODE2_ADDR`or `$BOOTNODE3_ADDR`) within `docker-compose.yaml` file:

```yaml
fullnode:
  image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--bootnodes=$BOOTNODE1_ADDR,$BOOTNODE2_ADDR,$BOOTNODE3_ADDR", "--datadir=/data", ...]
```

## Updating Q-Client & Docker Images

To upgrade the node follow the instructions [Upgrade Node](how-to-upgrade-node.md)

