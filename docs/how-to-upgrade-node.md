# Updating Q-Client & Docker Images

In case of severe updates of the Q-Client, you will be required to update the validator files and configs.
To do so, within directory `/validator` (for validator), `/rootnode` (for rootnode) or `/fullnode` (for fullnode),
use the following commands.

1. Go to directory `/validator` or `/rootnode` or `/fullnode`
/// details | Linux, macOS, other Unix-like systems

```bash
cd  "YOUR_DISK:/YOUR_DIRECTORY/mainnet-public-tools/validator" # or `/rootnode` or `/fullnode`
```
///

/// details | Windows

```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY\mainnet-public-tools\validator" # or `/rootnode` or `/fullnode`
```
///
<br>
2. Change the docker image directly in your `.env` file:
/// details | Linux, macOS, other Unix-like systems

```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

```
#This will open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .\env
```
///
<br>
```
...
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0
...
```
> **Note:**  the tag naming convention has been changed to include a v in the version tag. So, make sure to enter "...q-client:v1.4.0".

3. Pull (and overwrite) the latest docker image 
```bash
docker-compose pull
```

4. Restart with new configs & images
```bash
docker-compose down -t 60 && docker-compose up -d
```

Now your validator node should restart and synchronise with the testnet again.

