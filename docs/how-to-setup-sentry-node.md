# How to Setup Sentry Node


The sentry node setup is a way to secure your validator node by hiding it behind a set of so-called sentry fullnodes. The validator node is located in a private network and will only have contact with the sentry nodes. This tutorial is a brief description of how to achieve this.

1. Create machines for N sentry nodes and 1 validator node
2. Create private network for senty nodes' machines and validator's machine (e.g. you can use WireGuard and easy-wg-quick to quick setup)
3. Setup firewall on validator node to accept connections only from sentry nodes' machines (e.g. https://aws.amazon.com/network-firewall)
4. Setup sentry node(s):

  - Create directory `mainnet-sentrynode`
/// details | Linux, macOS, other Unix-like systems

1. Create a `mainnet-sentrynode` directory

```bash
mkdir mainnet-sentrynode
```
2. Go to the `mainnet-sentrynode` directory
```bash
cd "mainnet-sentrynode"
```
///

/// details | Windows

1. Create a `mainnet-sentrynode` directory

```bash
md mainnet-sentrynode
```
2. Go to the `mainnet-sentrynode` directory
```bash
cd "mainnet-sentrynode"
```
///
<br>

  - Create a `docker-compose.yaml` in the `mainnet-sentrynode` directory
/// details | Linux, macOS, other Unix-like systems

1. Create a `docker-compose.yaml` with nano and paste configuration provided below
```bash
nano docker-compose.yaml
```
After editing `docker-compose.yaml`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

1. Create `docker-compose.yaml` with notepad and paste configuration provided below
```bash
#This will create and open the docker-compose.yaml file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe docker-compose.yaml
```
///
<br>

```yaml
version: "3"

services:
  sentrynode:
    image: $QCLIENT_IMAGE
    entrypoint: [
       "geth",
       "--datadir=/data",
       "--bootnodes=$BOOTNODE_URI",
       "--port=$EXT_PORT",
       "--syncmode=full",
       "--verbosity=3",
       "--miner.gasprice=50000000000",
       "--txpool.pricelimit=47619047619",  
       "--http.addr=0.0.0.0",
       "--http.corsdomain=*",  
       "--http.api=net,web3,eth,debug,gov"
     ]
     volumes:
       - ./additional:/data/additional
       - sentrynode-data:/data
     ports:
       - $EXT_PORT:$EXT_PORT/tcp
       - $EXT_PORT:$EXT_PORT/udp
     restart: unless-stopped

volumes:
  sentrynode-data:
```
  - Create `.env` file
/// details | Linux, macOS, other Unix-like systems

1. Create a `.env` with nano and paste environment variables provided below
```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

1. Create `.env` with notepad and paste configuration provided below
```bash
#This will create and open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .env
```
///
<br>

```text
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0
BOOTNODE_URI=enode://$BOOTNODE_ENODE_PUBLIC_KEY@$BOOTNODE_IP:30301
EXT_PORT=30303
```

5. Run node
```bash
docker-compose up -d
```

6. Get admin.nodeInfo.enode from nodes' JS console and copy enode URI
```bash
docker-compose exec sentrynode geth attach data/geth.ipc

admin.nodeInfo.enode
```

7. Setup validator node:

  - Create directory `mainnet-validator`
/// details | Linux, macOS, other Unix-like systems

1. Go back from `mainnet-sentrynode`
```bash
cd  ..
```
2. Create a `mainnet-validator` directory

```bash
mkdir mainnet-validator
```
3. Go to the `mainnet-validator` directory
```bash
cd "mainnet-validator"
```
///

/// details | Windows
1. Go back from `mainnet-sentrynode`
```bash
cd  ..
```
2. Create a `mainnet-validator` directory

```bash
md mainnet-validator
```
3. Go to the `mainnet-validator` directory
```bash
cd "mainnet-validator"
```
///
<br>

  - Create `docker-compose.yaml` in the `mainnet-validator` directory
/// details | Linux, macOS, other Unix-like systems

1. Create a `docker-compose.yaml` with nano and paste configuration provided below
```bash
nano docker-compose.yaml
```
After editing `docker-compose.yaml`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

1. Create `docker-compose.yaml` with notepad and paste configuration provided below
```bash
#This will create and open the docker-compose.yaml file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe docker-compose.yaml
```
///
<br>
```yaml
version: "3"

services:
  validator-node:
    image: $QCLIENT_IMAGE
    entrypoint: [
      "geth",
      "--datadir=/data",
      "--bootnodes=$SENTRY",
      "--syncmode=full",
      "--verbosity=3",
      "--miner.gasprice=50000000000",
      "--txpool.pricelimit=47619047619",  
      "--mine",
      "--unlock=$VALIDATOR_ADDRESS",
      "--password=/data/passwd.txt",
      "--nodiscover",
      "--netrestrict $MASK"
    ]
    volumes:
      - ./keystore:/data/keystore
      - ./additional:/data/additional
      - validator-node-data:/data
    restart: unless-stopped

      
volumes:
  validator-node-data:
```

  - Create `.env` file
/// details | Linux, macOS, other Unix-like systems

1. Create a `.env` with nano and paste environment variables provided below
```bash
nano .env
```
After editing `.env`, save the changes in nano:

1. Press `Ctrl+O` to save changes
2. Press `Ctrl+X` to exit
///

/// details | Windows

1. Create `.env` with notepad and paste configuration provided below
```bash
#This will create and open the .env file in Notepad for editing. If you prefer to use a different text editor, replace notepad.exe with the appropriate command for your editor.
notepad.exe .env
```
///
<br>
```text
QCLIENT_IMAGE=qblockchain/q-client:v1.4.0
VALIDATOR_ADDRESS=0000000000000000000000000000000000000000
SENTRY=uri1,uri2,uri3
MASK=10.xxx.xxx.0/24
```

10.xxx.xxx.0/24 is the network mask of your private network (note: it can be not only in 10.0.0.0 range).
Sentry uri must be set for the node started in the previous step.

8. Add sentries' uris to `/data/geth/static-nodes.json`
```json
[
    "uri1",
    "uri2",
    "uri3",
    ...
]
```

uri1,uri2,uri3 are the enode uris of your sentry nodes. Please provide all uris because peer discovery discovery is disabled!

9. Start validator
```bash
docker-compose up -d
```

10. Wait for sync and check mining is working

> **Note:** if ping is huge your blocks can don't reach to blockchain
