# How to setup the OmniBridge

## Basic Requirements

OmniBridge is a component that allows to bridge assets from other blockchains (e.g. Ethereum mainnet, ..) onto Q. This is used for bridging and transferring collaterals for the creation of Q synthetic assets. Due to their role within Q, validators are predisposed to run an OmniBridge instance.

First please make sure you have a running validator node as described [here](how-to-setup-validator.md).

Also please make sure that you have the private key of the validator account as you need it in the config later. If you have a keystore file, you can use [extract-get-private-key.js + manual](https://gitlab.com/q-dev/mainnet-public-tools/-/tree/master/js-tools) to extract your private key from it.

## Download Repo and Configuration
Clone the repository

/// details | Linux
If you use Graphical User Interface(GUI), [open terminal](https://www.geeksforgeeks.org/how-to-open-terminal-in-linux/)

1. Press `CTRL + ALT + T` to open the terminal
2. Or open Show Application in the Dash Bar & find out Terminal Application.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/mainnet-public-tools
```
///

/// details | Windows (if you don't have git installed):
Open [PowerShell](https://learn.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7.5)

1. Press `Windows+R` to open the "Run" tool
2. Type "powershell" and press Enter to open terminal
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DISK:\YOUR_DIRECTORY"
```
If you work with cmd instead of PowerShell, use command:
```bash
cd /d "YOUR_DISK:\YOUR_DIRECTORY"
```
Replace YOUR_DISK and YOUR_DIRECTORY with real names.<br>
You can print the current working directory and its contents using the command:
```bash
dir
```


4. Clone the repository(PowerShell)
```
# Download the contents of the Git repository
Invoke-WebRequest -Uri https://gitlab.com/q-dev/mainnet-public-tools/-/archive/master/mainnet-public-tools-master.zip -OutFile mainnet-public-tools-master.zip

# Extract the contents of the ZIP file
Expand-Archive -Path mainnet-public-tools-master.zip -DestinationPath .

# Remove the ZIP file
Remove-Item -Path mainnet-public-tools-master.zip
```
///

/// details | MacOS
[Open Terminal on Mac](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac)

1. Click the Launchpad icon in the Dock, type Terminal in the search field, then click Terminal.
2. In the Finder, open the /Applications/Utilities folder, then double-click Terminal.
3. Go to the directory where you want to push the repository:
```bash
cd "YOUR_DIRECTORY"
```
You can print your current working directory with:
```bash
pwd
```

4. Clone the repository
```bash
git clone https://gitlab.com/q-dev/mainnet-public-tools
```
///
<br>
It should contain, amongst others, the three following folders:

| **Directory Name** | **Description** |
|:--|:--|
| `/omnibridge-oracle` | The core Q OmniBridge client |
| `/omnibridge-ui` | A graphical user interface to use the bridge on your local server |
| `/omnibridge-alm` | The monitor component to track the status of bridge transactions on the blockchain |

## Configure OmniBridge Oracle

Go into the `/omnibridge-oracle` directory.

/// details | Linux, macOS, other Unix-like systems

```bash
cd  "testnet-public-tools/omnibridge-oracle"
```
///

/// details | Windows

```bash
cd "testnet-public-tools\omnibridge-oracle"
```
///
<br>
This directory contains a `docker-compose.yaml` file for quickly launching the bridge oracle and some example environment configurations.

Copy the file `.env.mainnet` locally to `.env`.

/// details | Linux, macOS, other Unix-like systems
```
cp .env.mainnet .env
```
///

/// details | Windows
```
# This will copy the .env.mainnet file to a new file named .env.
copy ".\env.mainnet" ".\env"
```
///
<br>


Adjust the following parameters in `.env`:

| **Parameter Name** | **Value** |
|:--|:--|
| `ORACLE_VALIDATOR_ADDRESS` | Provide your Q validator address. Example: 0xac8e5047d122f801... |
| `ORACLE_VALIDATOR_ADDRESS_PRIVATE_KEY` | Provide your Q validator private key. Example: a385db8296ceb9a.... |
| `COMMON_HOME_RPC_URL` | You can keep the default, use https://rpc.q.org or use the RPC endpoint of our own full node if you are operating one. |
| `COMMON_FOREIGN_RPC_URL` | Provide an RPC endpoint of a client of the blockchain on the other side of the bridge. Q mainnet bridges to the Ethereum mainnet. You can use your own ethereum client, a public endpoint or [create an infura account](https://infura.io/) for free to get a personal Ethereum mainnet access point (e.g.  https://mainnet.infura.io/v3/1673abc....). |

## Launch the Oracle
To start the client with docker-compose make the call

```bash
$ docker-compose up -d
```

You can track the status of your bridge client by following the logs:

```bash
$ docker-compose logs -f --tail 100
```
To stop viewing logs, press `Ctrl+C`

## Setup and launch the OmniBridge-UI

Now change to the folder `/omnibridge-ui`

/// details | Linux, macOS, other Unix-like systems

```bash
cd  "../omnibridge-ui"
```
///

/// details | Windows

```bash
cd "..\omnibridge-ui"
```
///
<br>
and copy the file `.env.mainnet` to `.env`.

/// details | Linux, macOS, other Unix-like systems
```
cp .env.mainnet .env
```
///

/// details | Windows
```
# This will copy the .env.mainnet file to a new file named .env.
copy ".\env.mainnet" ".\env"
```
///
<br>
Adjust the following parameters in `.env`:

| **Parameter Name** | **Value** |
|:--|:--|
| REACT_APP_FOREIGN_RPC_URL | Provide an RPC endpoint of a client of the blockchain on the other side of the bridge, e.g. https://mainnet.infura.io/v3/1673abc.... |

Start the service

```bash
$ docker-compose up -d
```

You can find the UI running on your machine on port `:8080`, try accessing it with a browser.

## Setup and launch the Omnibridge-ALM


Now change to the folder `/omnibridge-alm`

/// details | Linux, macOS, other Unix-like systems

```bash
cd  "../omnibridge-alm"
```
///

/// details | Windows

```bash
cd "..\omnibridge-alm"
```
///
<br>
and copy the file `.env.mainnet` to `.env`.

/// details | Linux, macOS, other Unix-like systems
```
cp .env.mainnet .env
```
///

/// details | Windows
```
# This will copy the .env.mainnet file to a new file named .env.
copy ".\env.mainnet" ".\env"
```
///
<br>
Adjust the following parameters in `.env`:

| **Parameter Name** | **Value** |
|:--|:--|
| PORT | You can keep the default `8090` or change to some other port. |
| COMMON_HOME_RPC_URL | You can keep the default, use https://rpc.q.org or use the RPC endpoint of our own full node if you are operating one. |
| COMMON_FOREIGN_RPC_URL | Provide an RPC endpoint of a client of the blockchain on the other side of the bridge, e.g. https://mainnet.infura.io/v3/1673abc.... |
| ALM_HOME_EXPLORER_TX_TEMPLATE | You can keep the default https://explorer.q.org/tx/%s or change with the IP of your own full node block explorer. |

Start the monitor service with docker-compose

```bash
$ docker-compose up -d
```

You can find the monitor running on your machine on port `:8090`, try accessing it with a browser. It can be used to look for bridge transactions once the bridge is in use.
