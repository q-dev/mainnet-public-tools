# WQ Contract Addresses

This reference lists all the contract addresses of the Wrapped Q (WQ) token. These addresses are essential for interacting with the WQ tokens across various blockchain networks. Ensure to use the correct address for each network to avoid any issues with transactions.

| Network        | Contract Address                           |
|----------------|--------------------------------------------|
| Q Protocol     | 0xd07178e3eCbC78De110Df84fe1A979D5f349784a |
| Ethereum       | 0xb933e6003Fc9b9152Ac64C26F09779825cE4cb66 |
| Arbitrum       | 0xAed882F117b78034829E2cfFA11206706837B1b1 |
| Optimism       | 0xAed882F117b78034829E2cfFA11206706837B1b1 |

Wrapping and unwrapping of Q tokens on Q Protocol can be done via [Wrapped.q.org](https://wrapped.q.org/).