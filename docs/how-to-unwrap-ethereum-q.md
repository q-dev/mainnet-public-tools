# How to Transform Wrapped Q Tokens on Ethereum into Q Tokens on Q Blockchain

### Step 1: Access the Q Bridge
Go to [Q Bridge](https://bridge.q.org/).

### Step 2: Connect Your Wallet
Connect with your wallet that holds Wrapped Q on Ethereum and ensure you are connected to the Ethereum Network.

### Step 3: Select Wrapped Q Token
On the left side, click on “Wrapped BTC” and choose “Wrapped Q” (WQ).
Note: If WQ is not visible, click on “Add Custom Token” and use this Token Contract Address: 0xb933e6003Fc9b9152Ac64C26F09779825cE4cb66. The Token Symbol and Decimals of Precision should be filled out automatically. Press “Add Token”.

### Step 4: Enter WQ Token Balance
Enter the WQ token balance that you want to bridge to Q.

### Step 5: Unlock the Transaction
Press “Unlock” and confirm the transaction in your wallet.  
Note: This step will require some ETH for gas fees.

### Step 6: Transfer Tokens
Press “Transfer” and confirm the transaction in your wallet.  
Note: This step will require some ETH for gas fees.

### Step 7: Wait for Finalization
Wait for the bridge transaction to finalize.

### Step 8: Swap WQ for Q Tokens
To swap WQ for Q tokens, go to [Wrapped.q.org](https://wrapped.q.org/).

### Step 9: Connect Your Wallet to Q Mainnet
Connect with the same wallet that you used for the bridging transaction and ensure you are connected to “Q Mainnet”. If you cannot find “Q Mainnet” in your wallet, head over to [ChainList](https://chainlist.org/chain/35441), connect your wallet, and add Q Mainnet with a single click.

### Step 10: Enter WQ Amount
Enter the WQ amount that you want to swap into Q tokens.

### Step 11: Unwrap Tokens
Click on the green “Unwrap” button and confirm the transaction in your wallet.  
Note: This step will require some Q for gas fees. You should have received some Q tokens from the bridging transaction. If not, visit [Get Q](https://q.org/build/get-q).

### Step 12: Utilize Your Q Tokens
Finally, you can use your Q tokens on Q Mainnet.

## Utilizing Q Tokens

Serving as the native asset of the Q blockchain, Q tokens offer several key [utilities](https://q.org/blog/the-power-of-q-tokens-fueling-the-q-blockchain-ecosystem) to token holders. Additionally, Q token holders can participate in incentive programs to earn extra rewards. You can find Q token incentive programs [here](https://q.org/incentive-programs).