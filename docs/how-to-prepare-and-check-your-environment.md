## Setup your Server

The Q Node is required to run on a server or a (virtual) machine. One possibility is to use a local machine, alternatively you can use a cloud instance on AWS for example. A good external tutorial on how to create a cloud instance on AWS can be found [here](https://www.geeksforgeeks.org/amazon-ec2-creating-an-elastic-cloud-compute-instance/). Be careful to choose the right instance type and storage size according to the hardware requirements, for example `m4.xlarge` with 4 CPU and 16 RAM. 

Also, you should open the custom port range 30300-30400 to connect to nodes from outside and 8000-8999 to run an RPC and WSS endpoint. Any other machine will work as well if it meets the following requirements:

- Linux machine with SSH access;
- Installed applications: docker, docker-compose, git (optional).

Guide how to install [docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)
and [docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04) could be found
by the links.

#### Hardware Requirments

| | **Full Node** | **Validator Node** | **Root Node** |
|:--|:--|:--|:--|
| Disk | Min. 100 GB **free** disk space <br> Rec. 200 GB **free** disk space|Min. 35 GB **free** disk space <br> Rec. 50 GB **free** disk space| Min. 35 GB **free** disk space <br> Rec. 50 GB **free** disk space |
| Memory | Min. 10 GB RAM <br> Rec. 12 GB RAM | Min. 4 GB RAM <br> Rec. 6 GB RAM | Min. 4 GB RAM <br> Rec. 6 GB RAM |  
| CPU | Min. 1(v) Core(x86) <br> Rec. 2(v) Cores(x86) | Min. 1(v) Core(x86) <br> Rec. 2(v) Cores(x86) | Min. 1(v) Core(x86) <br> Rec. 2(v) Cores(x86) |
| Bandwidth | 24Mbps+ | 24Mbps+ | 24Mbps+ |
 
### Check your setup

Open the terminal and go to the directory with the node you will run (mainnet-public-tools/mainnet-fullnode or mainnet-rootnode or mainnet-validator):

/// details | Linux
If you use Graphical User Interface(GUI), [open terminal](https://www.geeksforgeeks.org/how-to-open-terminal-in-linux/)

1. Press `CTRL + ALT + T` to open the terminal
2. Or open Show Application in the Dash Bar & find out Terminal Application.
3. Go to directory with command:
```bash
cd "path/to/mainnet-public-tools/fullnode" # or rootnode, validator
```
///

/// details | Windows
Open [PowerShell](https://learn.microsoft.com/en-us/powershell/scripting/windows-powershell/starting-windows-powershell?view=powershell-7.5)

1. Press `Windows+R` to open the "Run" tool
2. Type "powershell" and press Enter to open terminal
3. Go to directory with command:
```bash
Set-Location -Path "mainnet-public-tools\fullnode" # or rootnode, validator
```
If you work with cmd instead of PowerShell, use command:
```bash
cd /d "YOUR_DISK:\mainnet-public-tools\fullnode" # or rootnode, validator
```
///


/// details | MacOS
[Open Terminal on Mac](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac)

1. Click the Launchpad icon in the Dock, type Terminal in the search field, then click Terminal.
2. In the Finder, open the /Applications/Utilities folder, then double-click Terminal.
3. Go to directory with command:
```bash
cd "path/to/mainnet-public-tools/fullnode" # or rootnode, validator
```
///

<br>
Check that your node has access to outside:

```bash
docker-compose run --rm --entrypoint "ping 8.8.8.8" fullnode # example for fullnode setup
```
Expected result:

    Creating miner1_node_run ... done
    PING 8.8.8.8 (8.8.8.8): 56 data bytes
    64 bytes from 8.8.8.8: seq=0 <...>
    64 bytes from 8.8.8.8: seq=1 <...>
    64 bytes from 8.8.8.8: seq=2 <...>
    64 bytes from 8.8.8.8: seq=3 <...>

Press `Ctrl+C` to stop.

Possible errors:

```text
1. Error: Permission denied 
   Solution: to fix this run docker command with sudo or update docker permissions by sudo usermod -aG docker $USER
2. Error: Can't find a suitable configuration file in this directory or any parent
   Solution: switch to the directory that contains your docker-compose file, e.g. cd mainnet-public-tools/mainnet-fullnode
```

Now you are ready to setup your node. Detailed guide you may find here for [fullnode](how-to-setup-fullnode.md), [rootnode](how-to-setup-rootnode.md), [validator](how-to-setup-validator.md).
